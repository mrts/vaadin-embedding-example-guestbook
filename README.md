# Example application that demonstrates embedding Vaadin web components

Forked from <https://github.com/mstahv/guestbook-embedding-example>,
uses the Vaadin plain Java starter project base.

## Additions to `mstahv/guestbook-embedding-example`

1. Rebuild the project base from <https://github.com/vaadin/skeleton-starter-flow> using Vaadin v23.3.0.
2. Add web component dynamic loading, open <http://localhost:8080/dynamic-loader> in the browser to try it.

## Usage and overview

Run using `mvn jetty:run` and open <http://localhost:8080/basic.html> or <http://localhost:8080/dynamic-loader> in the browser.

If you want to run your app locally in production mode, run `mvn jetty:run -Pproduction`.

The project follows Maven's [standard directory layout structure](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html).
