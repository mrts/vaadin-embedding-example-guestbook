import {LitElement, html, css, customElement} from 'lit-element';
import '@vaadin/vaadin-ordered-layout/src/vaadin-vertical-layout.js';
import '@vaadin/button/src/vaadin-button.js';

@customElement('dynamic-webcomponent-loader')
export class DynamicWebcomponentLoader extends LitElement {
    static get styles() {
        return css`
      :host {
          display: block;
          height: 100%;
      }
      `;
    }

    render() {
        return html`
            <vaadin-vertical-layout style="width: 100%; height: 100%; margin: var(--lumo-space-m);" theme="spacing-xs">
                <div style="font-weight: bold">
                    Dynamic web component loader
                </div>
                <div>
                    Press the button to load the web component:
                </div>
                <vaadin-button tabindex="0" id="loadWebComponent">
                    Load web component
                </vaadin-button>
                <div id="content"></div>
            </vaadin-vertical-layout>
        `;
    }

    registerAndLoadCustomElement(name: string) {
        const src: string = `/web-component/${name}.js`;
        const scriptId: string = `${name}-script`;

        console.log(`Loading custom element ${name}`)

        // Dynamically insert the element and script if they don't already exist.
        if (!document.getElementById(scriptId)) {
            const script = document.createElement('script');
            script.src = src;
            script.id = scriptId;
            document.body.appendChild(script);

            const element: HTMLElement = document.createElement(name);
            document.getElementById("content")?.appendChild(element);
        }
    }

    // Remove this method to render the contents of this view inside Shadow DOM
    createRenderRoot() {
        return this;
    }
}
