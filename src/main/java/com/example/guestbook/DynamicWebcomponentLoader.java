package com.example.guestbook;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.littemplate.LitTemplate;
import com.vaadin.flow.component.template.Id;
import com.vaadin.flow.router.Route;

/**
 * A Designer generated component for the dynamic-webcomponent-loader template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Route("dynamic-loader")
@Tag("dynamic-webcomponent-loader")
@JsModule("./src/dynamic-webcomponent-loader.ts")
public class DynamicWebcomponentLoader extends LitTemplate {

    @Id("loadWebComponent")
    private Button loadWebComponent;

    public DynamicWebcomponentLoader() {
        loadWebComponent.addClickListener(buttonClickEvent ->
                getElement().callJsFunction("registerAndLoadCustomElement", "guest-book"));
    }

}
